<?php
$redis = new Redis();
$dbconstr = 'pgsql:host=localhost;port=5432;dbname=cache;user=cache_user;password=cache_password';

$db = $_GET['db'];
if($db === null){ $db = 1; }
$tablename = 'cache'.$db;
$rediskey = 'db'.$db.'.versions';

$dbh = null;

$url = $_GET['url'];

$version = $_GET['version'];
if($version !== null){
  $redis->connect('127.0.0.1');
  $prev_value = $redis->get($rediskey);
  
  $ch = curl_init($url);
  curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  $new_value = curl_exec($ch);
  curl_close($ch);
  $need_refresh = $new_value !== $prev_value;
  
  if($need_refresh){
    $dbh = new PDO($dbconstr);
    $sql = "TRUNCATE TABLE ".$tablename;
    $res = $dbh->prepare($sql);
    $res->execute();

    $redis->set($rediskey,$new_value);
  }
  echo $new_value;
  exit();
 }

if($dbh === null){
  $dbh = new PDO($dbconstr);
 }

function join_paths() {
  $paths = array();
  
  foreach (func_get_args() as $arg) {
    if ($arg !== '') { $paths[] = $arg; }
  }
  
  return preg_replace('#/+#','/',join(':', $paths));
                      }
  
  $h = parse_url($url);
  $scheme = $h['scheme'];
  $host = $h['host'];
  if(array_key_exists('port',$h)){
    $port = $h['port'];
  }else{
    if($scheme == 'http'){
      $port = 80;
    }elseif($scheme == 'https'){
      $port = 443;
    }
  }
  $path = $h['path'];
  $path = explode('/',$path);
  $path = array_slice($path,1);
  $path = implode(':',$path);
  
  $a = join_paths($port,$host,$path);

 $sql = "
                      SELECT hstore_to_json(headers),content
                      FROM ".$tablename."
                      WHERE id=".$dbh->quote($a)."
   ";
 $res = $dbh->prepare($sql);
 $res->execute();
 $res->bindColumn(2,$lob,PDO::PARAM_LOB);
 $has_result = false;
 while($row = $res->fetch(PDO::FETCH_ASSOC)){
   $headers = json_decode($row['hstore_to_json']);
   $body = stream_get_contents($row['content']);
   $has_result = true;
   break;
 }
 if(!$has_result){
   $ch = curl_init($url);
   curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
   curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
   curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
   curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
   curl_setopt($ch,CURLOPT_HEADER,true);
   $result = curl_exec($ch);
   $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
   curl_close($ch);
   $headers_str = substr($result,0,$header_size);
   $body = substr($result,$header_size);
   $headers_arr = array_slice(explode("\n",$headers_str),1);
   $headers = array();
   foreach($headers_arr as $header_arr){
     $header_pieces = explode(':',$header_arr);
     $key = trim($header_pieces[0]);
     if($key === ''){continue;}
     if($key === 'accept-ranges'){continue;}
     $values = array_slice($header_pieces,1);
     $value = trim(implode(':',$values));
     $headers[$key] = $value;
   }
   $hstore_tokens = array();
   foreach($headers as $key => $value){
     $hstore_token = '"'.$key.'"=>"'.str_replace('"','\"',$value).'"';
     array_push($hstore_tokens,$hstore_token);
   }
   $hstore_str = implode(',',$hstore_tokens);
   $sql = "
     INSERT INTO ".$tablename."
     (id,headers,content,created)
     VALUES
     (?,?,?,NOW())
     ";
   $res = $dbh->prepare($sql);
   $res->bindParam(1,$a);
   $res->bindParam(2,$hstore_str);
   $res->bindParam(3,$body,PDO::PARAM_LOB);
   $res->execute();
 }
 
 header('Access-Control-Allow-Origin: *');
 header('Access-Control-Allow-Credentials: true');
 foreach($headers as $key => $value){
   header($key,$value);
 }
 echo $body;
 
