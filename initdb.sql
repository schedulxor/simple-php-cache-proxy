-- CREATE EXTENSION hstore;
-- GRANT ALL PRIVILEGES ON cache1 TO cache_user;
CREATE TABLE cache1(id VARCHAR(255) PRIMARY KEY,headers HSTORE NOT NULL,content BYTEA NOT NULL,created TIMESTAMP WITH TIME ZONE NOT NULL);
